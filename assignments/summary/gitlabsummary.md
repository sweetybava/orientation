# Gitlab Basics

![Gitlab logo](https://amazicworld.com/wp-content/uploads/2020/06/gitlab.png)

**GitLab** is a web-based DevOps lifecycle tool that provides a Git-repository manager providing wiki, issue-tracking and continuous integration/continuous deployment pipeline features, using an open-source license.

It is similar to the **_Git,_** Gitlab also have Repository, Issues, Commit, Merge request.

## Gitlab Bsic Workflow
![Gitlab Basic Workflow](https://luci.criosweb.ro/wp-content/uploads/2016/04/git-flow.png)

## Markdown
**Gitlab** have the **_Markdown_** formatting features.
![Markdown](https://about.gitlab.com/images/markdown-guide/mou-screenshot-preview.png)