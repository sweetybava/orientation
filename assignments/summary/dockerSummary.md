# Docker
![Docker logo](https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQWS21ZVvtotbl3RPfraAOOo6ro5EWwFguafA&usqp=CAU)

**Docker** can build images automatically by reading the instructions from a  'Dockerfile'.

It is an open platform for developing, shipping and running applications.

Dockerfile is text document that contains all the commands a user could call on the command line to assemble an image. Users can create an automated build that executes several command-line instructions in succession.

Taking advantage of Docker's methodologies for shipping, testing and deploying code quickly.

## Docker Engine
**Docker engine is a client server application.
It have three major components are,
   1. **_Server_**   - Long running program called a daemon process.
   2. **_REST API_** - Specifies interfaces that program can use to talk to the daemon and instruct it what to do.
   3. **_CLI_**      - Command line interface client.

![Docker Engine](https://docs.docker.com/engine/images/engine-components-flow.png)

## Docker Container
Docker container based platform allows for highly portable workloads.
![Docker Container](https://www.docker.com/sites/default/files/d8/2018-11/docker-containerized-appliction-blue-border_2.png)

## Docker Architecture
![Docker architecture](https://geekflare.com/wp-content/uploads/2019/09/docker-architecture-609x270.png)

## Docker daemon
  * Daemon can also communicate with other daemons to manage Docker services.
  * It listens for Docker API requests and manages Docker objects such as images, containers, networks and volumes.

## Docker commands
![Docker commands](https://raw.githubusercontent.com/sangam14/dockercheatsheets/master/dockercheatsheet8.png)

## Docker container commands
![Docker container commands](https://raw.githubusercontent.com/sangam14/dockercheatsheets/master/dockercheatsheet1.png)
