# Git Basics

## What is Git?
![Git logo](https://techies-world.com/wp-content/uploads/2016/08/git_logo.png)

Git is a **_Distributed version control software_**, It is used to manage code and easily collaborate with teammates super simple.

It easily track your work and your team during the development of your software.

Git software is easy to install in Windows, Mac and Linux, Easily handle everything from small to very large projects with speed and efficiency.

## Git Basic Workflow
![Git basic workflow](https://buddy.works/blog/images/gitflow.png)
#### Repository:
**Repository** is a collection of files and folders, you are using git to track..
![Repository](https://docs.microsoft.com/en-us/azure/devops/learn/_img/git_repositories.png)
#### Merge:
**Merge** is related to a branch and ready to part of the primary codebase, It will get merged into the master branch..
![Merge](https://res.cloudinary.com/practicaldev/image/fetch/s--MEKaM3dY--/c_imagga_scale,f_auto,fl_progressive,h_900,q_auto,w_1600/https://cl.ly/430Q2w473e2R/Image%25202018-04-30%2520at%25201.07.58%2520PM.png)
#### Branch:
The main software is called the **master branch**, branches of the trees are well and it have separate instances of the code that is different from the main codebase.
![branch](https://backlog.com/app/themes/backlog-child/assets/img/guides/git/collaboration/branching_workflows_001.png)
#### Commit:
**Commit** only exist on your local machine until it is pushed to a remote repository, It is like you are taking picture/snapshot.
#### Master:
![Master](https://www.nobledesktop.com/image/gitresources/git-branches-merge.png)
#### Clone:
**Clone** takes the entire online repository and makes an exact copy of it on a local machine..
![Clone](https://www.techiedelight.com/wp-content/uploads/git-remote.png)
#### CI:
![CI](https://about.gitlab.com/images/blogimages/cicd_pipeline_infograph.png)

### Git Internals
Git has three main states are,
   * Modified
   * Staged 
   * Committed
 
 1. **_Modified_** is used to changed the file but have not committed it to   your repository.
 2. **_Staged_** means that you have marked a modified file in its current version to go into your next picture/snapshot.
 3. **_Committed_** is used to store the data safely in your local machine in the form of pictures/snapshots.
#### Local Repository:
All the committed files go to this tree of your repository.
#### Remote Repository:
It is the copy of your local repository but is stored in some server on the internet and all the changes are commit into the Local repository are not directly reflected into this tree..
![Repository](https://miro.medium.com/max/1540/1*5KXszV8UWQDXWf9XwKEaSQ.png)


